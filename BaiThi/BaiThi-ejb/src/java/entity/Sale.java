/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "sale")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sale.findAll", query = "SELECT s FROM Sale s")
    , @NamedQuery(name = "Sale.findBySINo", query = "SELECT s FROM Sale s WHERE s.sINo = :sINo")
    , @NamedQuery(name = "Sale.findBySalesmanID", query = "SELECT s FROM Sale s WHERE s.salesmanID = :salesmanID")
    , @NamedQuery(name = "Sale.findByProductID", query = "SELECT s FROM Sale s WHERE s.productID = :productID")
    , @NamedQuery(name = "Sale.findBySalesmanName", query = "SELECT s FROM Sale s WHERE s.salesmanName = :salesmanName")
    , @NamedQuery(name = "Sale.findByDos", query = "SELECT s FROM Sale s WHERE s.dos = :dos")})
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "SINo")
    private Integer sINo;
    @Column(name = "SalesmanID")
    private Integer salesmanID;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ProductID")
    private Integer productID;
    @Size(max = 255)
    @Column(name = "SalesmanName")
    private String salesmanName;
    @Size(max = 255)
    @Column(name = "DOS")
    private String dos;
    @JoinColumn(name = "ProductID", referencedColumnName = "ProductID", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Product product;

    public Sale() {
    }

    public Sale(Integer productID) {
        this.productID = productID;
    }

    public Integer getSINo() {
        return sINo;
    }

    public void setSINo(Integer sINo) {
        this.sINo = sINo;
    }

    public Integer getSalesmanID() {
        return salesmanID;
    }

    public void setSalesmanID(Integer salesmanID) {
        this.salesmanID = salesmanID;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getDos() {
        return dos;
    }

    public void setDos(String dos) {
        this.dos = dos;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productID != null ? productID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sale)) {
            return false;
        }
        Sale other = (Sale) object;
        if ((this.productID == null && other.productID != null) || (this.productID != null && !this.productID.equals(other.productID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Sale[ productID=" + productID + " ]";
    }
    
}
