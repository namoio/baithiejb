create database EJB
create table sale(
SINo int ,
SalesmanID int,
ProductID int PRIMARY KEY  not null,
SalesmanName nvarchar(255),
DOS nvarchar(255),
FOREIGN KEY (ProductID) REFERENCES product(ProductID)
)

create table product(
ProductID int PRIMARY KEY  not null,
ProdName nvarchar(50),
Description nvarchar(50),
DateOfManf nvarchar(50)
)


